package com.example.eugen.garantii;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class CustomProductsDialog {

  public void showDialog(Activity activity, String msg){
    final Dialog dialog = new Dialog(activity);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setCancelable(true);
    dialog.setContentView(R.layout.custom_products_dialog);

    TextView id = dialog.findViewById(R.id.productsID);
    id.setText(msg);

    TextView actionOK = dialog.findViewById(R.id.actionOK);

    actionOK.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
    dialog.show();
  }
}
