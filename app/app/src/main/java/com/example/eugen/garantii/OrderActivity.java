package com.example.eugen.garantii;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class OrderActivity extends AppCompatActivity {

  Button infoSerial, addProducts, addPhoto, addVideo;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_order);


    infoSerial = findViewById(R.id.serialInfo);
    addProducts = findViewById(R.id.addProduct);
    addPhoto = findViewById(R.id.addPhoto);
    addVideo = findViewById(R.id.addVideo);

    Bundle serialInfo = getIntent().getExtras();
    String serialNumber = serialInfo.getString("serialInfo");

    infoSerial.setText(serialNumber);

    addPhoto.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent photo = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(photo, 0);
      }
    });
    addVideo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent video = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        video.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        if (video.resolveActivity(getPackageManager()) != null) {
          startActivityForResult(video, 1);
        }
      }
    });
    addProducts.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent product = new Intent(OrderActivity.this, ProductActivity.class);
        startActivity(product);
      }
    });
  }
}
