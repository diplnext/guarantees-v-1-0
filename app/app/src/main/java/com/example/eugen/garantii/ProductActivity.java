package com.example.eugen.garantii;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ProductActivity extends AppCompatActivity {

  String productID, denProduct, countProduct;
  ListView listProducts;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_product);

    listProducts = findViewById(R.id.listProducts);



    String url = "http://eugen.agro.md/api/return-products.php";
    NetworkGetRequest request = new NetworkGetRequest();
    request.run(url, new Callback() {
      @Override
      public void onFailure(Call call, IOException e) {
        e.printStackTrace();
      }

      @Override
      public void onResponse(Call call, Response response) throws IOException {
        if (!response.isSuccessful()) {
          throw new IOException("Unexpected code" + response);
        }
        final String responseData = response.body().string();
        ProductActivity.this.runOnUiThread(new Runnable() {
          @Override
          public void run() {
            try {
              JSONArray response = new JSONArray(responseData);
              if (response.length() == 0){
                Toast.makeText(ProductActivity.this, "Data didn't load", Toast.LENGTH_SHORT).show();
              } else {

                String[] productsID = new String[response.length()];
                String[] denProducts = new String[response.length()];
                String[] countProducts = new String[response.length()];

                for (int i = 0; i < response.length(); i++){

                final JSONObject resp = response.getJSONObject(i);

                productID = resp.getString("Cod Marfa");
                denProduct = resp.getString("Denumire marfa");
                countProduct = resp.getString("cantitate marfa");

                  productsID[i] = productID;
                  denProducts[i] = denProduct;
                  countProducts[i] = countProduct;
                }
                CustomProductsList listitem = new CustomProductsList(ProductActivity.this, productsID, denProducts, countProducts);
                listProducts.setAdapter(listitem);

//                String asdasd= String.valueOf(listProducts.getAdapter().getItem(0));
//                TextView textView = findViewById(R.id.productID);
//                String asdsad = textView.getText().toString();
//                Log.d("ProductID", asdasd);

              }
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        });
      }
    });
  }
  public void productIDClick(View v){
    TextView tv = v.findViewById(R.id.productID);
    AlertDialog.Builder alert = new AlertDialog.Builder(ProductActivity.this);
    alert.setMessage(tv.getText()).setCancelable(true);
    alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.show();
  }
}
