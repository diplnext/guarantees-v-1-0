package com.example.guarantees;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomProductsList extends ArrayAdapter<String> {

  private final Context context;
  private final String[] productID, denProduct, countProduct, priceProduct;
  public View view;


  public CustomProductsList(Context context, String[] productID, String[] denProduct, String[] countProduct, String[] priceProduct) {
    super(context,-1, productID);

    this.context = context;
    this.productID = productID;
    this.denProduct = denProduct;
    this.countProduct = countProduct;
    this.priceProduct = priceProduct;

  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.custom_products_list, parent, false);

    TextView id = view.findViewById(R.id.productID);
    TextView den = view.findViewById(R.id.denProduct);
    TextView count = view.findViewById(R.id.cantProduct);
    TextView price = view.findViewById(R.id.priceProduct);



    id.setText(productID[position]);
    den.setText(denProduct[position]);
    count.setText(countProduct[position]);
    price.setText(priceProduct[position]);

    return view;
  }

}

