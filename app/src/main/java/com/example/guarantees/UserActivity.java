package com.example.guarantees;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


public class UserActivity extends AppCompatActivity {

  Toolbar userToolbar;
  String productID, denProduct, countProduct, priceProduct,
          clientName, clientPhone, clientAddress, clientDistance, clientLatitude, clientLongitude,
          serialNumber, phone, address, orderID, techName, orderDate, orderName;
  ListView listProducts, listOrders;
  JSONArray orders;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_user);

    userToolbar = findViewById(R.id.userToolbar);

    ImageView userImage = findViewById(R.id.userImage);

    userImage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent imagePicker = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(imagePicker,1);
      }
    });

    listProducts = findViewById(R.id.listProducts);
    listOrders = findViewById(R.id.listOrders);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
      setSupportActionBar(userToolbar);
    }
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    ProgressDialog loading = new ProgressDialog(UserActivity.this);
    loading.setMessage("Loading... Please Wait");
    loading.setCancelable(false);
    loading.show();

            try {
              if (((ApplicationVars)getApplication()).getJSONArrayProducts().length() == 0){
                Toast.makeText(UserActivity.this, "Data didn't load", Toast.LENGTH_SHORT).show();
              } else {

                String[] productsID = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];
                String[] denProducts = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];
                String[] countProducts = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];
                String[] priceProducts = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];

                for (int i = 0; i < ((ApplicationVars)getApplication()).getJSONArrayProducts().length(); i++){

                  final JSONObject resp = ((ApplicationVars)getApplication()).getJSONArrayProducts().getJSONObject(i);

                  ((ApplicationVars)getApplication()).setProductID(resp.getString("Cod Marfa"));
                  ((ApplicationVars)getApplication()).setDenProduct(resp.getString("Denumire marfa"));
                  ((ApplicationVars)getApplication()).setCountProduct(resp.getString("cantitate marfa"));
                  ((ApplicationVars)getApplication()).setPriceProduct(resp.getString("pret marfa"));

                  productsID[i] = ((ApplicationVars)getApplication()).getProductID();
                  denProducts[i] = ((ApplicationVars)getApplication()).getDenProduct();
                  countProducts[i] = ((ApplicationVars)getApplication()).getCountProduct();
                  priceProducts[i] = ((ApplicationVars)getApplication()).getPriceProduct();
                }
                CustomProductsList listitem = new CustomProductsList(UserActivity.this, productsID, denProducts, countProducts, priceProducts);
                listProducts.setAdapter(listitem);

              }
            } catch (JSONException e) {
              e.printStackTrace();
            }
    final Context context = UserActivity.this.getApplicationContext();
    List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
    if (subscriptionInfos == null || subscriptionInfos.isEmpty()) {
      Toast.makeText(context, "No sims installed!", Toast.LENGTH_SHORT).show();
    } else {
      for (SubscriptionInfo subscriptionInfo : subscriptionInfos) {
        final String simSerialNumber = subscriptionInfo.getIccId();
        String apiUrl = getString(R.string.api_url) + simSerialNumber;
        NetworkGetRequest networkGetRequest = new NetworkGetRequest();
        networkGetRequest.run(apiUrl, new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            e.printStackTrace();
          }

          @Override
          public void onResponse(Call call, Response response) throws IOException {
            if (!response.isSuccessful()) {
              throw new IOException("Unexpected code" + response);
            }
            final String responseData = response.body().string();
            UserActivity.this.runOnUiThread(new Runnable() {
              @Override
              public void run() {
                try {
                  JSONObject resp = new JSONObject(responseData);
                  if (resp.length() == 0) {
                  } else {

                    orders = resp.getJSONArray("zaeavki");
                    String[] ordersID = new String[orders.length()];
                    String[] ordersDate = new String[orders.length()];
                    String[] ordersName = new String[orders.length()];
                    String[] clientsName = new String[orders.length()];
                    String[] clientsPhones = new String[orders.length()];
                    String[] techNames = new String[orders.length()];
                    String[] serialNumbers = new String[orders.length()];
                    String[] clientsAddress = new String[orders.length()];
                    String[] clientsDistance = new String[orders.length()];
                    for (int i = 0; i < orders.length(); i++) {

                      final JSONObject order = orders.getJSONObject(i);
                      orderID = order.getString("id");
                      orderDate = order.getString("data");
                      orderName = order.getString("tip order");
                      clientName = order.getString("client_name");
                      clientPhone = order.getString("contact_nr");
                      clientLatitude = order.getString("lat");
                      clientLongitude = order.getString("long");
                      techName = order.getString("tehnica");
                      serialNumber = order.getString("shasi");
                      clientDistance = order.getString("distance");
                      clientAddress = order.getString("location_str");

                      ordersID[i] = orderID;
                      ordersDate[i] = orderDate;
                      ordersName[i] = orderName;
                      clientsName[i] = clientName;
                      clientsPhones[i] = clientPhone;
                      techNames[i] = techName;
                      serialNumbers[i] = serialNumber;
                      clientsAddress[i] = clientAddress;
                      clientsDistance[i] = clientDistance;
                    }
                    CustomMainList listItem = new CustomMainList(UserActivity.this, clientsName, clientsPhones, clientsAddress, clientsDistance, serialNumbers, ordersID, ordersDate, ordersName, techNames);
                    listOrders.setAdapter(listItem);
                  }
                } catch (JSONException e) {
                  e.printStackTrace();
                }
              }
            });
          }
        });
      }
    }
    if (loading.isShowing()){
      loading.dismiss();
    }
  }
  protected void onActivityResult(int requestCode, int resultCode, Intent data){
    Bundle bundle = data.getExtras();
    Bitmap image = (Bitmap) bundle.get("data");
    ImageView userImage = findViewById(R.id.userImage);
    userImage.setImageBitmap(image);
  }
  public void productIDClick(View v){
    TextView tv = v.findViewById(R.id.productID);
    AlertDialog.Builder alert = new AlertDialog.Builder(UserActivity.this);
    alert.setMessage(tv.getText()).setCancelable(true);
    alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.show();
  }
  public void denProductClick(View v){
    TextView tv = v.findViewById(R.id.denProduct);
    AlertDialog.Builder alert = new AlertDialog.Builder(UserActivity.this);
    alert.setMessage(tv.getText()).setCancelable(true);
    alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.show();
  }
  public void showProducts(View v){
    View view;
    view = findViewById(R.id.listLayout);
    View listProducts =  view.findViewById(R.id.listProducts);
    View listOrders = view.findViewById(R.id.listOrders);
    listProducts.getVisibility();
    listOrders.getVisibility();
    if (listProducts.getVisibility() == View.VISIBLE){} else {
        listProducts.setVisibility(View.VISIBLE);
        listOrders.setVisibility(View.GONE);
    }
  }
  public void showStatistics(View v){
    View view;
    view = findViewById(R.id.listLayout);
    View listProducts = view.findViewById(R.id.listProducts);
    View listOrders = view.findViewById(R.id.listOrders);
    if (listOrders.getVisibility() == View.VISIBLE) {}else {
        listOrders.setVisibility(View.VISIBLE);
        listProducts.setVisibility(View.GONE);
    }
  }
}
