package com.example.guarantees;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomMainList extends ArrayAdapter<String> {

  private final Context context;
  private final String[] clientName, clientPhone, clientAddress,
          clientDistance, serialNumber, orderID, orderDate, orderName, techName;
  public View view;


  public CustomMainList(Context context, String[] clientName, String[] clientPhone, String[] clientAddress,
                         String[] clientDistance, String[] serialNumber, String[] orderID, String[] orderDate,
                        String [] orderName, String[] techName) {
    super(context, -1, clientName);
    this.context = context;
    this.clientName = clientName;
    this.clientPhone = clientPhone;
    this.clientAddress = clientAddress;
    this.clientDistance = clientDistance;
    this.serialNumber = serialNumber;
    this.orderName = orderName;
    this.orderID = orderID;
    this.orderDate = orderDate;
    this.techName = techName;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.list_view_item, parent, false);
    TextView OrderId = view.findViewById(R.id.idOrder);
    TextView OrderDate = view.findViewById(R.id.dateOrder);
    TextView OrderName = view.findViewById(R.id.nameOrder);
    TextView Name = view.findViewById(R.id.clientName);
    TextView Phone = view.findViewById(R.id.clientPhone);
    TextView TechName = view.findViewById(R.id.techName);
    TextView Serial = view.findViewById(R.id.serialNumber);
//    TextView Address = view.findViewById(R.id.clientAddress);
//    TextView Distance = view.findViewById(R.id.clientDistance);

    Name.setText(clientName[position]);
    Phone.setText(clientPhone[position]);
    Serial.setText(serialNumber[position]);
    OrderId.setText(orderID[position]);
    OrderDate.setText(orderDate[position]);
    OrderName.setText(orderName[position]);
    TechName.setText(techName[position]);
//    Address.setText(clientAddress[position]);
//    Distance.setText(clientDistance[position]);

    return view;
  }
}
