package com.example.guarantees;

import android.app.Application;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApplicationVars extends Application {

  // User Variables

  private String firstName;
  private String lastName;

  // Orders Variables

  private String orderID, orderDate, orderName, clientName, clientPhone, clientLatitude, clientLongitude, techName, serialNumber, clientDistance, clientAddress;
  private JSONObject JSONObjectOrders;

  // Products Variables

  private String productID, denProduct, countProduct, priceProduct;
  private JSONArray JSONArrayProducts;

  // Get Variables

  // User Variables

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  // Orders Variables

  public String getOrderID() {
    return orderID;
  }

  public String getOrderDate() {
    return orderDate;
  }

  public String getOrderName() {
    return orderName;
  }

  public String getclientName() {
    return clientName;
  }

  public String getClientPhone() {
    return clientPhone;
  }

  public String getClientLatitude() {
    return clientLatitude;
  }

  public String getClientLongitude() {
    return clientLongitude;
  }

  public String getTechName() {
    return techName;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public String getClientDistance() {
    return clientDistance;
  }

  public String getClientAddress() {
    return clientAddress;
  }

  public JSONObject getJSONObjectOrders() {
    return JSONObjectOrders;
  }

  // Products Variables

  public String getProductID(){
    return productID;
  }

  public String getDenProduct(){
    return denProduct;
  }

  public String getCountProduct() {
    return countProduct;
  }

  public String getPriceProduct() {
    return priceProduct;
  }

  public JSONArray getJSONArrayProducts() {
    return JSONArrayProducts;
  }

  // Set Variables

  // User Variables

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  // Orders Variables

  public void setOrderID(String orderID) {
    this.orderID = orderID;
  }

  public void setOrderDate(String orderDate) {
    this.orderDate = orderDate;
  }

  public void setOrderName(String orderName) {
    this.orderName = orderName;
  }

  public void setClientPhone(String clientPhone) {
    this.clientPhone = clientPhone;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public void setClientLatitude(String clientLatitude) {
    this.clientLatitude = clientLatitude;
  }

  public void setClientLongitude(String clientLongitude) {
    this.clientLongitude = clientLongitude;
  }

  public void setTechName(String techName) {
    this.techName = techName;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public void setClientDistance(String clientDistance) {
    this.clientDistance = clientDistance;
  }

  public void setClientAddress(String clientAddress) {
    this.clientAddress = clientAddress;
  }

  public void setJSONObjectOrders(JSONObject JSONObjectOrders){
    this.JSONObjectOrders =JSONObjectOrders;
  }

  // Products Variables

  public void setProductID(String productID) {
    this.productID = productID;
  }

  public void setDenProduct(String denProduct) {
    this.denProduct = denProduct;
  }

  public void setCountProduct(String countProduct) {
    this.countProduct = countProduct;
  }

  public void setPriceProduct(String priceProduct) {
    this.priceProduct = priceProduct;
  }

  public void setJSONArrayProducts(JSONArray JSONArrayProducts) {
    this.JSONArrayProducts = JSONArrayProducts;
  }

  public boolean allStringsNull(){
    if(
         this.orderID == null && this.orderDate == null && this.orderName == null && this.clientName == null &&
         this.clientPhone == null && this.techName == null && this.serialNumber == null
    ) return true;
    else
      return false;
  }

}
