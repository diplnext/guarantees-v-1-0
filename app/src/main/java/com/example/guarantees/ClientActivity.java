package com.example.guarantees;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ClientActivity extends AppCompatActivity {

  TextView clientName, clientIDNP, clientSold, clientSoldText;

  String techName, serialNumber;
  String[] techNames, serialNumbers;
  ListView clientProductList;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_client);

    clientName = findViewById(R.id.clientName);
    clientIDNP = findViewById(R.id.clientIDNP);
    clientSold = findViewById(R.id.sold);
    clientSoldText = findViewById(R.id.soldText);

    clientProductList = findViewById(R.id.clientProductList);

    Bundle bundle = getIntent().getExtras();
    String clientNameString = bundle.getString("clientName");
    String clientPhoneString = bundle.getString("clientPhone");
    final String techNameString = bundle.getString("techName");
    final String serialNumberString = bundle.getString("techSerialNumber");

    clientName.setText(clientNameString);

    String url = "http://eugen.agro.md/api/return-client.php?client_phone=" + clientPhoneString;
    NetworkGetRequest networkGetRequest = new NetworkGetRequest();
    networkGetRequest.run(url, new Callback() {
      @Override
      public void onFailure(Call call, IOException e) {
        e.printStackTrace();
      }

      @Override
      public void onResponse(Call call, Response response) throws IOException {
        if (!response.isSuccessful()){
          throw new IOException("Unexpected code: " + response);
        }
        final String responseData = response.body().string();
        ClientActivity.this.runOnUiThread(new Runnable() {
          @Override
          public void run() {
            try {
              JSONObject resp = new JSONObject(responseData);
              if (resp.length() == 0){
                Toast.makeText(ClientActivity.this, "Load Failed", Toast.LENGTH_SHORT).show();
              } else {
                JSONObject clientData = resp.getJSONObject("client");

                String clientIDNPString = clientData.getString("client_idnp");
                String clientSoldString = clientData.getString("client_sold");

                clientIDNP.setText(clientIDNPString);

                float clientSoldInt = Float.parseFloat(clientSoldString);
                clientSold.setText(String.valueOf(clientSoldInt));

                if (clientSoldInt < 0){
                  clientSold.setText(String.valueOf((clientSoldInt * -1)));
                  clientSold.setTextColor(getResources().getColor(R.color.colorRed));
                  clientSoldText.setText("Datorie: ");
                  clientSoldText.setTextColor(getResources().getColor(R.color.colorRed));
                }

                JSONArray clientTech = clientData.getJSONArray("client_tech");

                techNames = new String[clientTech.length()];
                serialNumbers = new String[clientTech.length()];

                for (int i = 0; i < clientTech.length(); i++){
                  JSONObject tech = clientTech.getJSONObject(i);

                  techName = tech.getString("tech_name");
                  serialNumber = tech.getString("serial_number");

                  techNames[i] = techName;
                  serialNumbers[i] = serialNumber;
                }
                final CustomClientProductList list = new CustomClientProductList(ClientActivity.this, techNames, serialNumbers, serialNumberString, techNameString );
                clientProductList.setAdapter(list);

                clientProductList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                  @Override
                  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView clientSerialNumber = view.findViewById(R.id.clientSerialNumber);
                    TextView clientTechName = view.findViewById(R.id.clientTechName);

                    String clientSerialNumberStr = clientSerialNumber.getText().toString();
                    String clientTechNameStr = clientTechName.getText().toString();

                    Intent productIntent = new Intent(ClientActivity.this, ActivityProduct.class);
                    productIntent.putExtra("techName", clientTechNameStr);
                    productIntent.putExtra("serialNumber", clientSerialNumberStr);
                    startActivity(productIntent);
                  }
                });
              }
            } catch (JSONException e){
              e.printStackTrace();
            }
          }
        });
      }
    });
  }
}
