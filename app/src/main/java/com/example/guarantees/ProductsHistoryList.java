package com.example.guarantees;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ProductsHistoryList extends ArrayAdapter<String> {
  private final Context context;
  private final String[] productsID, productsCode, productsDen, productsCount, productsPrice;
  public View view;

  public ProductsHistoryList(Context context, String[] productsID, String[] productsCode,
                             String[] productsDen, String[] productsCount, String[] productsPrice) {
    super(context, -1, productsID);
    this.context = context;
    this.productsID = productsID;
    this.productsCode = productsCode;
    this.productsDen = productsDen;
    this.productsCount = productsCount;
    this.productsPrice = productsPrice;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.custom_products_list_item, parent, false);

    TextView productIDTextView = view.findViewById(R.id.productID);
    TextView productCodeTextView = view.findViewById(R.id.productCode);
    TextView productDenTextView = view.findViewById(R.id.productDen);
    TextView productCountTextView = view.findViewById(R.id.productCount);
    TextView productPriceTextView = view.findViewById(R.id.productPrice);

    productIDTextView.setText(productsID[position]);
    productCodeTextView.setText(productsCode[position]);
    productDenTextView.setText(productsDen[position]);
    productCountTextView.setText(productsCount[position]);
    productPriceTextView.setText(productsPrice[position]);

    return view;
  }
}
