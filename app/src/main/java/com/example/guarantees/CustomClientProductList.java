package com.example.guarantees;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomClientProductList extends ArrayAdapter<String> {
  private final Context context;
  private final String[] techNames, serialNumbers;
  public View view;
  private String currentSelected, techNameString;

  public CustomClientProductList(Context context,String[] techNames, String[] serialNumbers, String currentSelected, String techNameString) {
    super(context, -1, techNames);

    this.context = context;
    this.techNames = techNames;
    this.serialNumbers = serialNumbers;
    this.currentSelected = currentSelected;
    this.techNameString = techNameString;
  }
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.custom_client_product_list, parent, false);

    TextView techNameTextView = view.findViewById(R.id.clientTechName);
    TextView serialNumberTextView = view.findViewById(R.id.clientSerialNumber);

    techNameTextView.setText(techNames[position]);
    serialNumberTextView.setText(serialNumbers[position]);
    if( currentSelected.equals(serialNumbers[position])){
      techNameTextView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
    }

    return view;
  }
}
